package Intechnica_Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static java.awt.SystemColor.text;

/**
 * Created by Tayo on 14/07/2017.
 */
public class WebsiteHomePage {

    protected WebDriver driver;

    public WebsiteHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "/html/body/text()[1]")WebElement  welcomeMessage;



    public void assertWebsiteHomePageTitle(){
        System.out.println( driver.getTitle());
        assert(driver.getTitle()).equals("Perf test server 1");


    }


    public void assertWelcomeMessage(String test){


        Assert.assertTrue(welcomeMessage.getText().equals(text));
    }

    public void assertWebsiteHomePageDisplayed(){

        if (driver.getTitle().equals("Perf test server 1")){

             Assert.assertTrue(welcomeMessage.getText().equals(text));
        }else

            assert(driver.getTitle()).equals("TrafficDefender Holding Page");
    }


}
