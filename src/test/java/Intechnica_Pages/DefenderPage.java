package Intechnica_Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Tayo on 14/07/2017.
 */
public class DefenderPage {

    protected WebDriver driver;

    public DefenderPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }



    @FindBy(className = "odometer-value")public static WebElement odometer;


    public void assertDefenderPageTitle(){
       System.out.println( driver.getTitle());
       assert(driver.getTitle()).equals("TrafficDefender Holding Page");


    }


    public void assertOdometer(){

        assert(odometer).isDisplayed();


    }

        public void assertDefenderPageDisplayed(){

        if (driver.getTitle().equals("TrafficDefender Holding Page")){

            assert(odometer).isDisplayed();
        }else

            assert(driver.getTitle()).equals("Perf test server 1");
    }


}
