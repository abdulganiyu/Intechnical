package com.IntechnicaTools;

import com.base.BaseClase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;



public class StepDefn extends BaseClase {

    WebDriver driver = null;


    public StepDefn(){
        this.driver = Hooks.driver;

    }



    @Given("^I enter the URL and the server is free of request$")
    public void i_enter_the_URL_and_the_server_is_free_of_request() throws Throwable {
       driver.get("http://52.31.43.9/");
    }

    @Then("^Welcome message \"([^\"]*)\" is displayed$")
    public void welcome_message_is_displayed(String test) throws Throwable {

        websiteHomePage.assertWebsiteHomePageTitle();
        //websiteHomePage.assertWelcomeMessage(test);

    }

    @Then("^The Defender page should be displayed putting the request in queue$")
    public void the_Defender_page_should_be_displayed_putting_the_request_in_queue() throws Throwable {

        defenderPage.assertDefenderPageTitle();

    }

    @And("^The queue page returns a valid queue position through odometer$")
    public void the_queue_page_returns_a_valid_queue_position_through_odometer() throws Throwable {

        defenderPage.assertOdometer();

    }

    @Then("^When space becomes available in the system the user in the queue gains access to the website$")
    public void when_space_becomes_available_in_the_system_the_user_in_the_queue_gains_access_to_the_website() throws Throwable {

        websiteHomePage.assertWebsiteHomePageTitle();

    }





}
