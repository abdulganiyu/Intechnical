package com.base;

import com.IntechnicaTools.Hooks;
import Intechnica_Pages.DefenderPage;
import Intechnica_Pages.WebsiteHomePage;
import org.openqa.selenium.WebDriver;

/**
 * Created by Tayo on 14/07/2017.
 */
public class BaseClase {

    protected WebDriver driver;
    public DefenderPage defenderPage;
    public WebsiteHomePage websiteHomePage;



    public BaseClase(){
        this.driver = Hooks.driver;
        defenderPage = new DefenderPage(driver);
        websiteHomePage = new WebsiteHomePage(driver);


    }

}
